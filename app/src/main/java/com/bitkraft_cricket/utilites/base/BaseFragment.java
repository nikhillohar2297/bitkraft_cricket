package com.bitkraft_cricket.utilites.base;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.bitkraft_cricket.utilites.LoadingDialogFragment;

public class BaseFragment extends Fragment {

    @NonNull
    public final FragmentActivity getActivityNonNull() {
        FragmentActivity activity = this.getActivity();
        if (activity == null) {
            throw new RuntimeException("null returned from getActivity");
        } else {
            return activity;
        }
    }

    public void showLoading(LoadingDialogFragment fragment) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        fragment.show(transaction, LoadingDialogFragment.class.getSimpleName());
    }

    public void hideLoading(LoadingDialogFragment fragment) {
        fragment.dismissAllowingStateLoss();
    }
}
