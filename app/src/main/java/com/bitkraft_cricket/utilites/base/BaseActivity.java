package com.bitkraft_cricket.utilites.base;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.bitkraft_cricket.utilites.LoadingDialogFragment;

public abstract class BaseActivity extends AppCompatActivity {

    public abstract View getLayout();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
    }


    public void showLoading(LoadingDialogFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        fragment.show(transaction, LoadingDialogFragment.class.getSimpleName());
    }

    public void hideLoading(LoadingDialogFragment fragment) {
        fragment.dismissAllowingStateLoss();
    }
}
