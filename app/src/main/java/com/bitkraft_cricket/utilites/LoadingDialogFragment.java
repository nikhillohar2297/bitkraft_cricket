package com.bitkraft_cricket.utilites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentTransaction;

import com.bitkraft_cricket.R;

public class LoadingDialogFragment extends DialogFragment {

    private boolean isLoading;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_loading, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ProgressBar progressBar = view.findViewById(R.id.progress_circular);
        progressBar.animate();

    }

    @Override
    public int show(@NonNull FragmentTransaction transaction, @Nullable String tag) {
        if (!isLoading) {
            isLoading = true;
            return super.show(transaction, tag);
        } else {
            return 0;
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        if (isLoading) {
            super.dismissAllowingStateLoss();
            isLoading = false;
        }
    }

    @Override
    public int getTheme() {
        return R.style.FullScreenDialog;
    }
}
