package com.bitkraft_cricket.utilites.wrapper.network;

import android.content.Context;

import com.bitkraft_cricket.data.CountryData;

import java.util.ArrayList;
import java.util.List;

public class ConfigUtils {
    public ConfigData getConfigData(String destination) {
        List<ConfigData> countryDataList = getConfigList();
        for (ConfigData configData : countryDataList) {
            if (configData.getDestination().equals(destination)) {
                return configData;
            }
        }
        return null;
    }

    private List<ConfigData> getConfigList() {
        List<ConfigData> configDataList = new ArrayList<>();

        ConfigData configData1 = new ConfigData();
        configData1.setDestination("country-list");
        configData1.setConnectionPoint("https://api.jsonbin.io/b/5f5a07e17243cd7e82398c5b/latest");

        ConfigData configData2 = new ConfigData();
        configData2.setDestination("team-stats");
        configData2.setConnectionPoint("https://api.jsonbin.io/b/5f5a0a917243cd7e82398d7a/latest");

        ConfigData configData3 = new ConfigData();
        configData3.setDestination("player-list");
        configData3.setConnectionPoint("https://api.jsonbin.io/b/5f5a0d8fad23b57ef90f837c/latest");

        ConfigData configData4 = new ConfigData();
        configData4.setDestination("match-list");
        configData4.setConnectionPoint("https://api.jsonbin.io/b/5f5a1511ad23b57ef90f87f8/latest");


        configDataList.add(configData1);
        configDataList.add(configData2);
        configDataList.add(configData3);
        configDataList.add(configData4);

        return configDataList;
    }
}
