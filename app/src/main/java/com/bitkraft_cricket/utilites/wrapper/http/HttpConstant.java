package com.bitkraft_cricket.utilites.wrapper.http;


public class HttpConstant {
    public static final String RESP_ERROR_DATA = "error_data";
    public static final String RESP_SUCCESS = "success_data";

    public static final String RESPONSE_CODE = "response_code";
}
