package com.bitkraft_cricket.utilites.wrapper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.bitkraft_cricket.utilites.wrapper.http.HttpConnector;
import com.bitkraft_cricket.utilites.wrapper.http.HttpConstant;
import com.bitkraft_cricket.utilites.wrapper.network.ConfigUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class CommRouter {
    private static final String TAG = CommRouter.class.getSimpleName();

    private HttpConnector httpConnector;

    private String destination;

    private Context context;

    private ResponseData responseData;

    public CommRouter(Context context) {
        this.context = context;
        httpConnector = new HttpConnector(context);
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }


    public void sendToDestination(final CommCallback commCallback) {
        if (!isDeviceOnline()){
            JSONObject errorJson = new JSONObject();
            try {
                errorJson.put(HttpConstant.RESP_ERROR_DATA, "Please check you internet connection and try again");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setRequestData(errorJson, false);
            pushRequest(commCallback);
        }else {
            httpConnector.setConfigData(new ConfigUtils().getConfigData(destination));
            httpConnector.doHttpCall(new HttpConnector.HttpCallBack() {
                @Override
                public void onSuccess(JSONObject jsonObject) {
                    if (jsonObject != null) {
                        try {
                            processHttpCall(jsonObject, commCallback);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(JSONObject jsonObject) {

                    setRequestData(jsonObject, false);
                    pushRequest(commCallback);
                }
            });
        }
    }

    private void setRequestData(JSONObject jsonObject, boolean isStatus) {
        responseData = new ResponseData();
        responseData.setResponseJson(jsonObject);
        responseData.setStatus(isStatus);
    }

    private void pushRequest(CommCallback commCallback) {
        commCallback.onResult(responseData);
    }

    private void processHttpCall(JSONObject jsonObject, CommCallback commCallback) throws JSONException {
        int responseCode = jsonObject.getInt(HttpConstant.RESPONSE_CODE);

        switch (responseCode) {
            case 200:
                JSONObject successJson = new JSONObject();
                successJson.put(HttpConstant.RESP_SUCCESS, jsonObject);
                setRequestData(successJson, true);
                break;
            case 401:
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put(HttpConstant.RESP_ERROR_DATA, "User is unauthorized");
                setRequestData(jsonObject1, false);
                break;
            case 403:
                JSONObject jsonObject2 = new JSONObject();
                jsonObject2.put(HttpConstant.RESP_ERROR_DATA, "Cannot be found");
                setRequestData(jsonObject2, false);
                break;
            default:
                JSONObject jsonObject3 = new JSONObject();
                jsonObject3.put(HttpConstant.RESP_ERROR_DATA, "Something went wrong! try again later");
                setRequestData(jsonObject3, false);
                break;
        }

        pushRequest(commCallback);

    }

    public interface CommCallback {
        void onResult(ResponseData responseData);
    }
}
