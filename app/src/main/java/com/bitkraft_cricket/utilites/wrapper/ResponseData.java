package com.bitkraft_cricket.utilites.wrapper;

import org.json.JSONObject;

public class ResponseData {
    private boolean isStatus;
    private JSONObject responseJson;

    public boolean isStatus() {
        return isStatus;
    }

    public void setStatus(boolean status) {
        isStatus = status;
    }

    public JSONObject getResponseJson() {
        return responseJson;
    }

    public void setResponseJson(JSONObject responseJson) {
        this.responseJson = responseJson;
    }
}
