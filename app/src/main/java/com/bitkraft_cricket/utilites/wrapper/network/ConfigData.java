package com.bitkraft_cricket.utilites.wrapper.network;

public class ConfigData {
    private String destination;
    private String connectionPoint;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getConnectionPoint() {
        return connectionPoint;
    }

    public void setConnectionPoint(String connectionPoint) {
        this.connectionPoint = connectionPoint;
    }
}