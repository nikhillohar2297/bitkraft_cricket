package com.bitkraft_cricket.utilites.wrapper.http;

import android.content.Context;
import android.util.Log;

import com.bitkraft_cricket.utilites.wrapper.network.ConfigData;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;

public class HttpConnector {
    private static final String TAG = HttpConnector.class.getSimpleName();
    private Context context;

    private ConfigData configData;

    public HttpConnector(Context context) {
        this.context = context;
    }

    public void setConfigData(ConfigData configData) {
        this.configData = configData;
    }

    public void doHttpCall(final HttpCallBack callBack) {
        String url = null;

        Request.Builder builder = new Request.Builder();

        setHeader(builder);

        url = configData.getConnectionPoint();

        if (url != null) {
            builder.url(url);
        } else {
            Log.e(TAG, "Connection point is null");
        }

        getOkHttpClient()
                .newCall(builder.build())
                .enqueue(new Callback() {
                    @Override
                    public void onFailure(@NotNull Call call, @NotNull IOException e) {
                        JSONObject jsonObject;

                        try {
                            jsonObject = new JSONObject();
                            jsonObject.put(HttpConstant.RESP_ERROR_DATA, "");
                            callBack.onFailure(jsonObject);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }

                    @Override
                    public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                        JSONObject jsonObject;
                        if (!response.isSuccessful()) {

                            try {
                                jsonObject = new JSONObject();
                                jsonObject.put(HttpConstant.RESP_ERROR_DATA, "");
                                callBack.onFailure(jsonObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            int responseCode = response.code();

                            ResponseBody responseBody = response.body();

                            if (responseBody != null) {
                                Log.e(TAG, "Response body: " + responseBody.toString());
                                try {
                                    JSONObject responseObject = new JSONObject(responseBody.string());
                                    responseObject.put(HttpConstant.RESPONSE_CODE, responseCode);
                                    callBack.onSuccess(responseObject);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                });
    }

    private OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        // Add interceptors
        attachLoggingInterceptor(builder);

        return builder.build();
    }

    private void attachLoggingInterceptor(OkHttpClient.Builder builder) {
        // Logging interceptor
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        builder.addInterceptor(loggingInterceptor);
    }

    private void setHeader(Request.Builder builder) {
        builder.addHeader("secret-key", "$2b$10$/v37glTt6d/UHY6PfrLp4.X2Fnezxy1E3bGWc5gjX4chRjTE4bUXi");
    }

    public interface HttpCallBack {
        void onSuccess(JSONObject jsonObject);

        void onFailure(JSONObject jsonObject);
    }
}