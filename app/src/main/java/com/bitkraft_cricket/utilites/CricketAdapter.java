package com.bitkraft_cricket.utilites;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

public class CricketAdapter extends FragmentStateAdapter {

    private Fragment[] fragments;

    public CricketAdapter(Fragment fragment, Fragment[] fragments){
        this(fragment);
        this.fragments = fragments;
    }

    public CricketAdapter(@NonNull Fragment fragment) {
        super(fragment);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return fragments[position];
    }

    @Override
    public int getItemCount() {
        return fragments.length;
    }
}

