package com.bitkraft_cricket;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bitkraft_cricket.data.CountryData;
import com.bitkraft_cricket.databinding.LayoutCountryItemBinding;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<CountryData> countryDataList;

    private ICountryAdapter callBack;

    public static CountryAdapter newInstance(Context context, List<CountryData> countryDataList, ICountryAdapter callBack) {
        CountryAdapter countryAdapter = new CountryAdapter();
        countryAdapter.context = context;
        countryAdapter.countryDataList = countryDataList;
        countryAdapter.callBack = callBack;

        return countryAdapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutCountryItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_country_item, parent, false);

        return new ItemHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final ItemHolder itemHolder = (ItemHolder) holder;
        final CountryData countryData = countryDataList.get(position);

        itemHolder.binding.tvCountryName.setText(countryData.getName());

        itemHolder.binding.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onItemClick(countryData);
            }
        });


        Glide.with(itemHolder.binding.ivCountryIcon.getContext()).load(countryData.getIconUrl()).listener(new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                itemHolder.binding.loadingBar.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                itemHolder.binding.loadingBar.setVisibility(View.GONE);
                return false;
            }
        }).into(itemHolder.binding.ivCountryIcon);
    }

    static class ItemHolder extends RecyclerView.ViewHolder {
        private LayoutCountryItemBinding binding;

        public ItemHolder(@NonNull LayoutCountryItemBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

    @Override
    public int getItemCount() {
        return countryDataList.size();
    }

    public interface ICountryAdapter {
        void onItemClick(CountryData countryData);
    }
}
