package com.bitkraft_cricket;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bitkraft_cricket.data.CountryData;
import com.bitkraft_cricket.databinding.ActivityMainBinding;
import com.bitkraft_cricket.utilites.LoadingDialogFragment;
import com.bitkraft_cricket.utilites.base.BaseActivity;
import com.google.gson.Gson;

import java.util.List;

public class MainActivity extends BaseActivity {

    private ActivityMainBinding binding;
    private MainViewModel mainViewModel;

    private LoadingDialogFragment loadingDialogFragment;

    @Override
    public View getLayout() {
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this));
        return binding.getRoot();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadingDialogFragment = new LoadingDialogFragment();

        mainViewModel = new ViewModelProvider(getViewModelStore(), new ViewModelProvider.NewInstanceFactory()).get(MainViewModel.class);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.rvCountryList.setLayoutManager(layoutManager);

        binding.tabBar.tvTitle.setText("Country List");

        setUpcCountryList();
    }

    private void setUpcCountryList() {
        mainViewModel.getCountryList(this).observe(this, new Observer<List<CountryData>>() {
            @Override
            public void onChanged(List<CountryData> countryData) {
                if (countryData != null) {
                    CountryAdapter adapter = CountryAdapter.newInstance(MainActivity.this, countryData, new CountryAdapter.ICountryAdapter() {
                        @Override
                        public void onItemClick(CountryData countryData) {
                            Intent intent = new Intent(MainActivity.this, DashboardActivity.class);
                            intent.putExtra("country_data", new Gson().toJson(countryData));

                            startActivity(intent);
                        }
                    });

                    binding.rvCountryList.setAdapter(adapter);
                }
            }
        });

        mainViewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    showLoading(loadingDialogFragment);
                } else {
                    hideLoading(loadingDialogFragment);
                }
            }
        });

        mainViewModel.errorData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {
                    Log.e(MainActivity.class.getSimpleName(), "Error : " + s);

                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this)
                            .setCancelable(false)
                            .setTitle("Error")
                            .setMessage(s)
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    setUpcCountryList();
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    finish();
                                }
                            });
                    builder.create().show();
                }
            }
        });
    }
}