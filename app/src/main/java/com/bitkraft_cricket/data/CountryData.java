package com.bitkraft_cricket.data;

import com.google.gson.annotations.SerializedName;

public class CountryData {
    @SerializedName("name")
    private String name;

    @SerializedName("icon")
    private String iconUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
