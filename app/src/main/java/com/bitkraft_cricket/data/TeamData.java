package com.bitkraft_cricket.data;

import com.google.gson.annotations.SerializedName;

public class TeamData {

    @SerializedName("match_stats")
    private MatchStats matchStats;

    @SerializedName("cup_stats")
    private CupStats cupStats;

    @SerializedName("team_stats")
    private TeamStats teamStats;

    public static class MatchStats {
        @SerializedName("match_played")
        private long matchPlayer;
        @SerializedName("match_won")
        private long matchWon;
        @SerializedName("match_lost")
        private long matchLost;

        public long getMatchPlayer() {
            return matchPlayer;
        }

        public void setMatchPlayer(long matchPlayer) {
            this.matchPlayer = matchPlayer;
        }

        public long getMatchWon() {
            return matchWon;
        }

        public void setMatchWon(long matchWon) {
            this.matchWon = matchWon;
        }

        public long getMatchLost() {
            return matchLost;
        }

        public void setMatchLost(long matchLost) {
            this.matchLost = matchLost;
        }
    }

    public static class CupStats {
        @SerializedName("world_cup_won")
        private int worldCup;

        @SerializedName("t_20_won")
        private int t20Cup;

        public int getWorldCup() {
            return worldCup;
        }

        public void setWorldCup(int worldCup) {
            this.worldCup = worldCup;
        }

        public int getT20Cup() {
            return t20Cup;
        }

        public void setT20Cup(int t20Cup) {
            this.t20Cup = t20Cup;
        }
    }

    public static class TeamStats {
        @SerializedName("batting")
        private int batting;
        @SerializedName("bowling")
        private int bowling;
        @SerializedName("fielding")
        private int fielding;

        public int getBatting() {
            return batting;
        }

        public void setBatting(int batting) {
            this.batting = batting;
        }

        public int getBowling() {
            return bowling;
        }

        public void setBowling(int bowling) {
            this.bowling = bowling;
        }

        public int getFielding() {
            return fielding;
        }

        public void setFielding(int fielding) {
            this.fielding = fielding;
        }
    }

    public MatchStats getMatchStats() {
        return matchStats;
    }

    public void setMatchStats(MatchStats matchStats) {
        this.matchStats = matchStats;
    }

    public CupStats getCupStats() {
        return cupStats;
    }

    public void setCupStats(CupStats cupStats) {
        this.cupStats = cupStats;
    }

    public TeamStats getTeamStats() {
        return teamStats;
    }

    public void setTeamStats(TeamStats teamStats) {
        this.teamStats = teamStats;
    }
}


