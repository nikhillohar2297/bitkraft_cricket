package com.bitkraft_cricket.data;

import com.google.gson.annotations.SerializedName;

public class PlayerData {
    @SerializedName("player_name")
    private String playerName;

    @SerializedName("player_role")
    private String playerRole;

    @SerializedName("wk_data")
    private WicketKeepData wicketKeepData;

    @SerializedName("bowl_data")
    private BowlingData bowlingData;

    @SerializedName("bat_data")
    private BattingData battingData;

    public static class WicketKeepData {
        @SerializedName("is_wk")
        private boolean is_wk;

        public boolean isIs_wk() {
            return is_wk;
        }

        public void setIs_wk(boolean is_wk) {
            this.is_wk = is_wk;
        }
    }

    public static class BowlingData {
        @SerializedName("is_bowl")
        private boolean is_ball;

        @SerializedName("wicket")
        private int wicket;
        @SerializedName("median_over")
        private int medianOver;

        @SerializedName("hatrik")
        private int hatrik;

        public boolean isIs_ball() {
            return is_ball;
        }

        public void setIs_ball(boolean is_ball) {
            this.is_ball = is_ball;
        }

        public int getWicket() {
            return wicket;
        }

        public void setWicket(int wicket) {
            this.wicket = wicket;
        }

        public int getMedianOver() {
            return medianOver;
        }

        public void setMedianOver(int medianOver) {
            this.medianOver = medianOver;
        }

        public int getHatrik() {
            return hatrik;
        }

        public void setHatrik(int hatrik) {
            this.hatrik = hatrik;
        }
    }

    public static class BattingData {
        @SerializedName("is_batting")
        private boolean isBatting;

        @SerializedName("total_runs")
        private int totalRuns;

        @SerializedName("total_6s")
        private int totalSixes;

        @SerializedName("total_4")
        private int totalFours;

        public boolean isBatting() {
            return isBatting;
        }

        public void setBatting(boolean batting) {
            isBatting = batting;
        }

        public int getTotalRuns() {
            return totalRuns;
        }

        public void setTotalRuns(int totalRuns) {
            this.totalRuns = totalRuns;
        }

        public int getTotalSixes() {
            return totalSixes;
        }

        public void setTotalSixes(int totalSixes) {
            this.totalSixes = totalSixes;
        }

        public int getTotalFours() {
            return totalFours;
        }

        public void setTotalFours(int totalFours) {
            this.totalFours = totalFours;
        }
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerRole() {
        return playerRole;
    }

    public void setPlayerRole(String playerRole) {
        this.playerRole = playerRole;
    }

    public WicketKeepData getWicketKeepData() {
        return wicketKeepData;
    }

    public void setWicketKeepData(WicketKeepData wicketKeepData) {
        this.wicketKeepData = wicketKeepData;
    }

    public BowlingData getBowlingData() {
        return bowlingData;
    }

    public void setBowlingData(BowlingData bowlingData) {
        this.bowlingData = bowlingData;
    }

    public BattingData getBattingData() {
        return battingData;
    }

    public void setBattingData(BattingData battingData) {
        this.battingData = battingData;
    }
}
