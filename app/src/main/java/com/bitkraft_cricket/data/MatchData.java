package com.bitkraft_cricket.data;

import com.google.gson.annotations.SerializedName;

public class MatchData {

    @SerializedName("team_a")
    private String teamA;

    @SerializedName("team_a_icon")
    private String teamAIcon;


    @SerializedName("team_b")
    private String teamB;

    @SerializedName("team_b_icon")
    private String teamBIcon;

    @SerializedName("winner")
    private String winner;

    @SerializedName("looser")
    private String looser;

    @SerializedName("date")
    private String date;

    @SerializedName("man_of_match")
    private String manOfMatch;

    @SerializedName("bowler_of_match")
    private String bowlerOfMatch;

    @SerializedName("best_fielding")
    private String bestFielding;

    public String getTeamA() {
        return teamA;
    }

    public void setTeamA(String teamA) {
        this.teamA = teamA;
    }

    public String getTeamAIcon() {
        return teamAIcon;
    }

    public void setTeamAIcon(String teamAIcon) {
        this.teamAIcon = teamAIcon;
    }

    public String getTeamB() {
        return teamB;
    }

    public void setTeamB(String teamB) {
        this.teamB = teamB;
    }

    public String getTeamBIcon() {
        return teamBIcon;
    }

    public void setTeamBIcon(String teamBIcon) {
        this.teamBIcon = teamBIcon;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLooser() {
        return looser;
    }

    public void setLooser(String looser) {
        this.looser = looser;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getManOfMatch() {
        return manOfMatch;
    }

    public void setManOfMatch(String manOfMatch) {
        this.manOfMatch = manOfMatch;
    }

    public String getBowlerOfMatch() {
        return bowlerOfMatch;
    }

    public void setBowlerOfMatch(String bowlerOfMatch) {
        this.bowlerOfMatch = bowlerOfMatch;
    }

    public String getBestFielding() {
        return bestFielding;
    }

    public void setBestFielding(String bestFielding) {
        this.bestFielding = bestFielding;
    }
}
