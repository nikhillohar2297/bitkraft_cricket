package com.bitkraft_cricket;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.bitkraft_cricket.data.CountryData;
import com.bitkraft_cricket.databinding.ActivityDashboardBinding;
import com.bitkraft_cricket.matches.MatchFragment;
import com.bitkraft_cricket.team.DashboardTeamFragment;
import com.bitkraft_cricket.utilites.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;

public class DashboardActivity extends BaseActivity {

    private ActivityDashboardBinding binding;

    CountryData countryData;

    @Override
    public View getLayout() {
        binding = ActivityDashboardBinding.inflate(LayoutInflater.from(this));
        return binding.getRoot();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null) {
            countryData = new Gson().fromJson(getIntent().getStringExtra("country_data"), CountryData.class);
        }

        binding.include.tvTitle.setText(countryData.getName());
        binding.include.ivCountryIcon.setVisibility(View.VISIBLE);
        Glide.with(this).load(countryData.getIconUrl()).into(binding.include.ivCountryIcon);


        showDashboardTeam();

        binding.dashboardNav.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.teams:
                                showDashboardTeam();
                                break;
                            case R.id.matches:
                                showMatches();
                                break;
                        }
                        return true;
                    }
                }
        );

        binding.dashboardNav.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                Toast.makeText(DashboardActivity.this, item.getTitle().toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDashboardTeam(){
        DashboardTeamFragment teamFragment = new DashboardTeamFragment();
        showFragment(teamFragment);
    }

    private void showMatches(){
        MatchFragment fragment = new MatchFragment();
        showFragment(fragment);
    }

    private void showFragment(Fragment fragment){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();
    }
}