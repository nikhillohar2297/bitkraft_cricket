package com.bitkraft_cricket.team;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bitkraft_cricket.R;
import com.bitkraft_cricket.data.TeamData;
import com.bitkraft_cricket.databinding.FragmentTeamBinding;
import com.bitkraft_cricket.team.viewmodel.TeamViewModel;
import com.bitkraft_cricket.utilites.LoadingDialogFragment;
import com.bitkraft_cricket.utilites.base.BaseFragment;

public class TeamFragment extends BaseFragment {
    private FragmentTeamBinding binding;
    private TeamViewModel viewModel;
    private LoadingDialogFragment dialogFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_team, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(getViewModelStore(), new ViewModelProvider.NewInstanceFactory()).get(TeamViewModel.class);
        dialogFragment = new LoadingDialogFragment();

        setTeamData();
    }


    private void setTeamData() {
        viewModel.getTeamData(getActivityNonNull()).observe(getViewLifecycleOwner(), new Observer<TeamData>() {
            @Override
            public void onChanged(TeamData teamData) {
                if (teamData != null) {
                    setMatchStats(teamData.getMatchStats());
                    setCupStats(teamData.getCupStats());
                    setTeamStats(teamData.getTeamStats());
                }
            }
        });

        viewModel.isLoading().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    showLoading(dialogFragment);
                } else {
                    hideLoading(dialogFragment);
                }
            }
        });

        viewModel.errorData().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {
                    Log.e(this.getClass().getSimpleName(), "Error : " + s);

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivityNonNull())
                            .setCancelable(false)
                            .setTitle("Error")
                            .setMessage(s)
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    setTeamData();
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                    builder.create().show();
                }
            }
        });
    }

    private void setMatchStats(TeamData.MatchStats matchStats) {
        binding.matchStats.layoutMatchPlayed.tvTitle.setText("Match \nPlayed");
        binding.matchStats.layoutMatchPlayed.tvValue.setText(String.valueOf(matchStats.getMatchPlayer()));


        binding.matchStats.layoutMatchWon.tvTitle.setText("Match \nWon");
        binding.matchStats.layoutMatchWon.tvValue.setText(String.valueOf(matchStats.getMatchWon()));
        binding.matchStats.layoutMatchWon.tvTitle.setTextColor(getResources().getColor(R.color.colorWonGreen));
        binding.matchStats.layoutMatchWon.tvValue.setTextColor(getResources().getColor(R.color.colorWonGreen));


        binding.matchStats.layoutMatchLost.tvTitle.setText("Match \nLost");
        binding.matchStats.layoutMatchLost.tvValue.setText(String.valueOf(matchStats.getMatchLost()));
        binding.matchStats.layoutMatchLost.tvTitle.setTextColor(getResources().getColor(R.color.colorLoseRed));
        binding.matchStats.layoutMatchLost.tvValue.setTextColor(getResources().getColor(R.color.colorLoseRed));


        float matchValue = (float) matchStats.getMatchWon()/matchStats.getMatchPlayer();
        float percentValue = (float) (matchValue * 100);

        binding.matchStats.progressBar.setProgress((int) percentValue);
    }

    private void setCupStats(TeamData.CupStats cupStats) {
        binding.cupStats.tvValue1.setText(String.valueOf(cupStats.getWorldCup()));
        binding.cupStats.tvValue2.setText(String.valueOf(cupStats.getT20Cup()));
    }

    private void setTeamStats(TeamData.TeamStats teamStats) {
        binding.teamStats.battingProgress.setProgress(teamStats.getBatting());
        binding.teamStats.blowingProgress.setProgress(teamStats.getBowling());
        binding.teamStats.fieldingProgress.setProgress(teamStats.getFielding());

        binding.teamStats.tvBattingPercent.setText(String.valueOf(teamStats.getBatting()));
        binding.teamStats.tvBowlingPercent.setText(String.valueOf(teamStats.getBowling()));
        binding.teamStats.tvFieldingPercent.setText(String.valueOf(teamStats.getFielding()));
    }
}
