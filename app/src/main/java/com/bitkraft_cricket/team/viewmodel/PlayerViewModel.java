package com.bitkraft_cricket.team.viewmodel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bitkraft_cricket.MainViewModel;
import com.bitkraft_cricket.data.PlayerData;
import com.bitkraft_cricket.utilites.wrapper.CommRouter;
import com.bitkraft_cricket.utilites.wrapper.ResponseData;
import com.bitkraft_cricket.utilites.wrapper.http.HttpConstant;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PlayerViewModel extends ViewModel {
    private MutableLiveData<List<PlayerData>> playerListData = new MutableLiveData<>();
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private MutableLiveData<String> errorData = new MutableLiveData<>();

    public LiveData<List<PlayerData>> getPlayerData(Context context) {
        playerListData.setValue(null);
        isLoading.setValue(true);
        errorData.setValue(null);

        CommRouter commRouter = new CommRouter(context);
        commRouter.setDestination("player-list");
        commRouter.sendToDestination(new CommRouter.CommCallback() {
            @Override
            public void onResult(ResponseData responseData) {
                isLoading.postValue(false);

                if (!responseData.isStatus()) {
                    if (responseData.getResponseJson().has(HttpConstant.RESP_ERROR_DATA)) {
                        try {
                            String errorMessage = responseData.getResponseJson().getString(HttpConstant.RESP_ERROR_DATA);
                            errorData.postValue(errorMessage);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Log.e(MainViewModel.class.getSimpleName(), responseData.getResponseJson().toString());
                    if (responseData.getResponseJson().has(HttpConstant.RESP_SUCCESS)) {
                        try {
                            JSONObject responseJson = responseData.getResponseJson().getJSONObject(HttpConstant.RESP_SUCCESS);

                            if (responseJson.has("player_list")) {
                                try {
                                    processPlayerList(responseJson.getJSONArray("player_list"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        return playerListData;
    }

    private void processPlayerList(JSONArray jsonArray) {
        List<PlayerData> playerDataList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                PlayerData playerData = new Gson().fromJson(jsonObject.toString(), PlayerData.class);
                playerDataList.add(playerData);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        playerListData.postValue(playerDataList);
    }


    public LiveData<Boolean> isLoading() {
        return isLoading;
    }

    public LiveData<String> errorData() {
        return errorData;
    }
}
