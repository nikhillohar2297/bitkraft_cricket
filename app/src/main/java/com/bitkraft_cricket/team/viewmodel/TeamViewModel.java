package com.bitkraft_cricket.team.viewmodel;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.bitkraft_cricket.MainViewModel;
import com.bitkraft_cricket.data.TeamData;
import com.bitkraft_cricket.utilites.wrapper.CommRouter;
import com.bitkraft_cricket.utilites.wrapper.ResponseData;
import com.bitkraft_cricket.utilites.wrapper.http.HttpConstant;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class TeamViewModel extends ViewModel {
    private MutableLiveData<TeamData> teamData = new MutableLiveData<>();
    private MutableLiveData<Boolean> isLoading = new MutableLiveData<>();
    private MutableLiveData<String> errorData = new MutableLiveData<>();


    public LiveData<TeamData> getTeamData(Context context) {
        teamData.setValue(null);
        isLoading.setValue(true);
        errorData.setValue(null);

        CommRouter commRouter = new CommRouter(context);
        commRouter.setDestination("team-stats");
        commRouter.sendToDestination(new CommRouter.CommCallback() {
            @Override
            public void onResult(ResponseData responseData) {
                isLoading.postValue(false);

                if (!responseData.isStatus()) {
                    if (responseData.getResponseJson().has(HttpConstant.RESP_ERROR_DATA)) {
                        try {
                            String errorMessage = responseData.getResponseJson().getString(HttpConstant.RESP_ERROR_DATA);
                            errorData.postValue(errorMessage);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    if (responseData.getResponseJson().has(HttpConstant.RESP_SUCCESS)) {
                        try {
                            JSONObject responseJson = responseData.getResponseJson().getJSONObject(HttpConstant.RESP_SUCCESS);
                            TeamData data = new Gson().fromJson(responseJson.toString(), TeamData.class);
                            teamData.postValue(data);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    Log.e(MainViewModel.class.getSimpleName(), responseData.getResponseJson().toString());

                }
            }
        });

        return teamData;
    }

    public LiveData<Boolean> isLoading() {
        return isLoading;
    }

    public LiveData<String> errorData() {
        return errorData;
    }
}
