package com.bitkraft_cricket.team;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bitkraft_cricket.R;
import com.bitkraft_cricket.data.PlayerData;
import com.bitkraft_cricket.databinding.LayoutPlayerItemBinding;

import java.util.List;

public class PlayerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private IPlayerAdapter callBack;
    private List<PlayerData> playerDataList;

    public static PlayerAdapter newInstance(Context context, List<PlayerData> playerDataList, IPlayerAdapter callBack) {
        PlayerAdapter adapter = new PlayerAdapter();
        adapter.callBack = callBack;
        adapter.playerDataList = playerDataList;
        adapter.context = context;

        return adapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutPlayerItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_player_item, parent, false);

        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ItemViewHolder viewHolder = (ItemViewHolder) holder;
        final PlayerData playerData = playerDataList.get(position);

        viewHolder.binding.tvSrNo.setText(String.valueOf(position + 1));
        if (playerData.getPlayerRole() != null) {
            viewHolder.binding.tvPlayerName.setText(playerData.getPlayerName() + "(" + playerData.getPlayerRole() + ")");
        }else {
            viewHolder.binding.tvPlayerName.setText(playerData.getPlayerName());
        }

        viewHolder.binding.tvPlayerName.findFocus();

        viewHolder.binding.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onPlayerSelected(playerData);
            }
        });

        setBalling(viewHolder, playerData);
        setBatting(viewHolder, playerData);
        setWicketKeep(viewHolder, playerData);
    }

    private void setWicketKeep(ItemViewHolder holder, PlayerData playerData){
        if (playerData.getWicketKeepData() != null){
            if (playerData.getWicketKeepData().isIs_wk()){
                holder.binding.ivWKeeping.setVisibility(View.VISIBLE);
            }else {
                holder.binding.ivWKeeping.setVisibility(View.INVISIBLE);
            }
        }else {
            holder.binding.ivWKeeping.setVisibility(View.INVISIBLE);
        }
    }

    private void setBatting(ItemViewHolder holder, PlayerData playerData){
        if (playerData.getBattingData() != null){
            if (playerData.getBattingData().isBatting()){
                holder.binding.ivBat.setVisibility(View.VISIBLE);
            }else {
                holder.binding.ivBat.setVisibility(View.INVISIBLE);
            }
        }else {
            holder.binding.ivBat.setVisibility(View.INVISIBLE);
        }
    }

    private void setBalling(ItemViewHolder holder, PlayerData playerData){
        if (playerData.getBowlingData() != null){
            if (playerData.getBowlingData().isIs_ball()){
                holder.binding.ivBall.setVisibility(View.VISIBLE);
            }else {
                holder.binding.ivBall.setVisibility(View.INVISIBLE);
            }
        }else {
            holder.binding.ivBall.setVisibility(View.INVISIBLE);
        }
    }

    static class ItemViewHolder extends RecyclerView.ViewHolder {
        private LayoutPlayerItemBinding binding;

        public ItemViewHolder(@NonNull LayoutPlayerItemBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

    @Override
    public int getItemCount() {
        return playerDataList.size();
    }

    public interface IPlayerAdapter {
        void onPlayerSelected(PlayerData playerData);
    }
}
