package com.bitkraft_cricket.team;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.bitkraft_cricket.R;
import com.bitkraft_cricket.databinding.FragmentDashbaordTeamBinding;
import com.bitkraft_cricket.utilites.CricketAdapter;
import com.bitkraft_cricket.utilites.base.BaseFragment;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class DashboardTeamFragment extends BaseFragment {

    FragmentDashbaordTeamBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashbaord_team, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
    }

    private void init() {
        binding.viewPagerHome.setAdapter(createPagerAdapter());
        new TabLayoutMediator(binding.tabBarTeam, binding.viewPagerHome, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                if (position == 0){
                    tab.setText("Team Stats");
                }else if (position == 1){
                    tab.setText("Players");
                }
            }
        }).attach();
    }

    private CricketAdapter createPagerAdapter() {
        Fragment[] fragments = new Fragment[2];
        fragments[0] = new TeamFragment();
        fragments[1] = new PlayerFragment();

        return new CricketAdapter(this, fragments);
    }
}
