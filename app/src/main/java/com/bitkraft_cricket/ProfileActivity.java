package com.bitkraft_cricket;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import com.bitkraft_cricket.data.PlayerData;
import com.bitkraft_cricket.databinding.ActivityProfileDetailsBinding;
import com.bitkraft_cricket.utilites.base.BaseActivity;
import com.google.gson.Gson;

public class ProfileActivity extends BaseActivity {

    private ActivityProfileDetailsBinding binding;
    private PlayerData playerData;

    @Override
    public View getLayout() {
        binding = ActivityProfileDetailsBinding.inflate(LayoutInflater.from(this));

        return binding.getRoot();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding.include2.tvTitle.setText("Profile");
        binding.include2.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (getIntent() != null) {
            playerData = new Gson().fromJson(getIntent().getStringExtra("player_data"), PlayerData.class);
        }

        init();
    }

    private void init(){
        setWicketKeep();
        setBalling();
        setBatting();
        setPersonalInfo();
        setProfileStats();
    }

    private void setPersonalInfo() {

        if (playerData.getPlayerRole() != null) {
            binding.profileDetails.tvFullName.setText(playerData.getPlayerName() + "(" + playerData.getPlayerRole() + ")");
        } else {
            binding.profileDetails.tvFullName.setText(playerData.getPlayerName());
        }
    }

    private void setProfileStats() {
        binding.playerStats.totalWickets.tvTitle.setText("Total Wickets");
        if (playerData.getBowlingData() != null && playerData.getBowlingData().isIs_ball()) {
            binding.playerStats.totalWickets.tvValue.setText(String.valueOf(playerData.getBowlingData().getWicket()));
        } else {
            binding.playerStats.totalWickets.tvValue.setText("N.A");
        }


        binding.playerStats.totalRuns.tvTitle.setText("Total Runs");
        if (playerData.getBattingData() != null && playerData.getBattingData().isBatting()) {
            binding.playerStats.totalRuns.tvValue.setText(String.valueOf(playerData.getBattingData().getTotalRuns()));
        } else {
            binding.playerStats.totalRuns.tvValue.setText("N.A");
        }


        binding.playerStats.total6.tvTitle.setText("Total 6s");
        if (playerData.getBattingData() != null && playerData.getBattingData().isBatting()) {
            binding.playerStats.total6.tvValue.setText(String.valueOf(playerData.getBattingData().getTotalSixes()));
        } else {
            binding.playerStats.total6.tvValue.setText("N.A");
        }


        binding.playerStats.total4.tvTitle.setText("Total 4s");
        if (playerData.getBattingData() != null && playerData.getBattingData().isBatting()) {
            binding.playerStats.total4.tvValue.setText(String.valueOf(playerData.getBattingData().getTotalFours()));
        } else {
            binding.playerStats.total4.tvValue.setText("N.A");
        }
    }


    private void setWicketKeep() {
        if (playerData.getWicketKeepData() != null) {
            if (playerData.getWicketKeepData().isIs_wk()) {
                binding.ivWKeeping.setVisibility(View.VISIBLE);
            } else {
                binding.ivWKeeping.setVisibility(View.GONE);
            }
        } else {
            binding.ivWKeeping.setVisibility(View.GONE);
        }
    }

    private void setBatting() {
        if (playerData.getBattingData() != null) {
            if (playerData.getBattingData().isBatting()) {
                binding.ivBat.setVisibility(View.VISIBLE);
            } else {
                binding.ivBat.setVisibility(View.GONE);
            }
        } else {
            binding.ivBat.setVisibility(View.GONE);
        }
    }

    private void setBalling() {
        if (playerData.getBowlingData() != null) {
            if (playerData.getBowlingData().isIs_ball()) {
                binding.ivBall.setVisibility(View.VISIBLE);
            } else {
                binding.ivBall.setVisibility(View.GONE);
            }
        } else {
            binding.ivBall.setVisibility(View.GONE);
        }
    }
}
