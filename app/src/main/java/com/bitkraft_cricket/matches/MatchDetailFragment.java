package com.bitkraft_cricket.matches;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.bitkraft_cricket.R;
import com.bitkraft_cricket.data.MatchData;
import com.bitkraft_cricket.databinding.FragmentMatchDetailsBinding;
import com.bitkraft_cricket.utilites.base.BaseFragment;
import com.bumptech.glide.Glide;

public class MatchDetailFragment extends BaseFragment {
    private FragmentMatchDetailsBinding binding;
    private MatchData matchData;

    public MatchDetailFragment(MatchData matchData) {
        this.matchData = matchData;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_match_details, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
    }

    private void init() {
        setUpTeam();
        setUpBestPlayers();
        setUpResult();
    }

    private void setUpTeam() {
        binding.layoutTeam1.tvTeamName.setText(matchData.getTeamA());
        Glide.with(this).load(matchData.getTeamAIcon()).into(binding.layoutTeam1.ivTeamIcon);


        binding.layoutTeam2.tvTeamName.setText(matchData.getTeamB());
        Glide.with(this).load(matchData.getTeamBIcon()).into(binding.layoutTeam2.ivTeamIcon);

        setUpResult();
    }

    private void setUpBestPlayers() {
        binding.bestPlayerStats.layoutMom.tvTitle.setText("Man of the match");
        binding.bestPlayerStats.layoutBom.tvTitle.setText("Bowler of the match");
        binding.bestPlayerStats.layoutFom.tvTitle.setText("Fielder of the match");


        binding.bestPlayerStats.layoutMom.tvValue.setText(matchData.getManOfMatch());
        binding.bestPlayerStats.layoutBom.tvValue.setText(matchData.getBowlerOfMatch());
        binding.bestPlayerStats.layoutFom.tvValue.setText(matchData.getBestFielding());
    }


    private void setUpResult() {
        if (matchData.getTeamA().equals(matchData.getWinner())) {
            binding.layoutTeam1.cardResult.setCardBackgroundColor(getResources().getColor(R.color.colorWonGreen));
            binding.layoutTeam1.tvResult.setText("Winner");
        }


        if (matchData.getTeamB().equals(matchData.getWinner())) {
            binding.layoutTeam2.cardResult.setCardBackgroundColor(getResources().getColor(R.color.colorWonGreen));
            binding.layoutTeam2.tvResult.setText("Winner");
        }


        if (matchData.getTeamA().equals(matchData.getLooser())) {
            binding.layoutTeam1.cardResult.setCardBackgroundColor(getResources().getColor(R.color.colorLoseRed));
            binding.layoutTeam1.tvResult.setText("Looser");
        }


        if (matchData.getTeamB().equals(matchData.getLooser())) {
            binding.layoutTeam2.cardResult.setCardBackgroundColor(getResources().getColor(R.color.colorLoseRed));
            binding.layoutTeam2.tvResult.setText("Looser");
        }
    }
}
