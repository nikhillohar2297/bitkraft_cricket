package com.bitkraft_cricket.matches;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bitkraft_cricket.R;
import com.bitkraft_cricket.data.MatchData;
import com.bitkraft_cricket.databinding.LayoutMatchItemBinding;
import com.bumptech.glide.Glide;

import java.util.List;

public class MatchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<MatchData> matchDataList;
    private IMatchAdapter callBack;

    public static MatchAdapter newInstance(Context context, List<MatchData> matchDataList, IMatchAdapter callBack) {
        MatchAdapter adapter = new MatchAdapter();
        adapter.context = context;
        adapter.matchDataList = matchDataList;
        adapter.callBack = callBack;

        return adapter;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutMatchItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_match_item, parent, false);

        return new ItemHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ItemHolder viewHolder = (ItemHolder) holder;
        final MatchData matchData = matchDataList.get(position);

        viewHolder.binding.layoutTeam1.tvTeamName.setText(matchData.getTeamA());
        Glide.with(context).load(matchData.getTeamAIcon()).into(viewHolder.binding.layoutTeam1.ivTeamIcon);


        viewHolder.binding.layoutTem2.tvTeamName.setText(matchData.getTeamB());
        Glide.with(context).load(matchData.getTeamBIcon()).into(viewHolder.binding.layoutTem2.ivTeamIcon);

        if (matchData.getTeamA().equals(matchData.getWinner())) {
            viewHolder.binding.layoutTeam1.teamData.setCardBackgroundColor(context.getResources().getColor(R.color.colorWonGreen));
        }


        if (matchData.getTeamB().equals(matchData.getWinner())) {
            viewHolder.binding.layoutTem2.teamData.setCardBackgroundColor(context.getResources().getColor(R.color.colorWonGreen));
        }


        if (matchData.getTeamA().equals(matchData.getLooser())) {
            viewHolder.binding.layoutTeam1.teamData.setCardBackgroundColor(context.getResources().getColor(R.color.colorLoseRed));
        }


        if (matchData.getTeamB().equals(matchData.getLooser())) {
            viewHolder.binding.layoutTem2.teamData.setCardBackgroundColor(context.getResources().getColor(R.color.colorLoseRed));
        }

        viewHolder.binding.tvMatchDate.setText(matchData.getDate());

        viewHolder.binding.matchItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callBack.onMatchSelected(matchData);
            }
        });
    }

    static class ItemHolder extends RecyclerView.ViewHolder {
        private LayoutMatchItemBinding binding;

        public ItemHolder(@NonNull LayoutMatchItemBinding binding) {
            super(binding.getRoot());

            this.binding = binding;
        }
    }

    @Override
    public int getItemCount() {
        return matchDataList.size();
    }

    public interface IMatchAdapter {
        void onMatchSelected(MatchData matchData);
    }
}