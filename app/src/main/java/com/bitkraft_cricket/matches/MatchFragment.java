package com.bitkraft_cricket.matches;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bitkraft_cricket.R;
import com.bitkraft_cricket.data.MatchData;
import com.bitkraft_cricket.databinding.FragmentMatchesBinding;
import com.bitkraft_cricket.matches.viewmodel.MatchViewModel;
import com.bitkraft_cricket.utilites.LoadingDialogFragment;
import com.bitkraft_cricket.utilites.base.BaseFragment;
import com.google.gson.Gson;

import java.util.List;

public class MatchFragment extends BaseFragment {
    private FragmentMatchesBinding binding;
    private MatchViewModel viewModel;
    private LoadingDialogFragment dialogFragment;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_matches, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(getViewModelStore(), new ViewModelProvider.NewInstanceFactory()).get(MatchViewModel.class);
        dialogFragment = new LoadingDialogFragment();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivityNonNull());
        binding.rvMatches.setLayoutManager(layoutManager);

        setMatchList();
    }

    private void setMatchList(){
        viewModel.getMatchData(getActivityNonNull()).observe(getViewLifecycleOwner(), new Observer<List<MatchData>>() {
            @Override
            public void onChanged(List<MatchData> matchDataList) {
                if (matchDataList != null){
                    MatchAdapter adapter = MatchAdapter.newInstance(getActivityNonNull(), matchDataList, new MatchAdapter.IMatchAdapter() {
                        @Override
                        public void onMatchSelected(MatchData matchData) {
                            Intent intent = new Intent(getActivityNonNull(), MatchDetailActivity.class);
                            intent.putExtra("match_data", new Gson().toJson(matchData));

                            startActivity(intent);
                        }
                    });

                    binding.rvMatches.setAdapter(adapter);
                }
            }
        });

        viewModel.isLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    showLoading(dialogFragment);
                } else {
                    hideLoading(dialogFragment);
                }
            }
        });

        viewModel.errorData().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s != null) {
                    Log.e(this.getClass().getSimpleName(), s);


                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivityNonNull())
                            .setCancelable(false)
                            .setTitle("Error")
                            .setMessage(s)
                            .setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    setMatchList();
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                    builder.create().show();
                }
            }
        });
    }
}
