package com.bitkraft_cricket.matches;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.Nullable;

import com.bitkraft_cricket.R;
import com.bitkraft_cricket.data.MatchData;
import com.bitkraft_cricket.databinding.ActivityMatchDetailsBinding;
import com.bitkraft_cricket.utilites.base.BaseActivity;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

public class MatchDetailActivity extends BaseActivity {

    private ActivityMatchDetailsBinding binding;
    private MatchData matchData;

    @Override
    public View getLayout() {
        binding = ActivityMatchDetailsBinding.inflate(LayoutInflater.from(this));

        return binding.getRoot();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null){
            matchData = new Gson().fromJson(getIntent().getStringExtra("match_data"), MatchData.class);
        }

        binding.include3.tvTitle.setText("Team details");
        binding.include3.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        init();
    }

    private void init() {
        setUpTeam();
        setUpBestPlayers();
        setUpResult();
    }

    private void setUpTeam() {
        binding.layoutTeam1.tvTeamName.setText(matchData.getTeamA());
        Glide.with(this).load(matchData.getTeamAIcon()).into(binding.layoutTeam1.ivTeamIcon);


        binding.layoutTeam2.tvTeamName.setText(matchData.getTeamB());
        Glide.with(this).load(matchData.getTeamBIcon()).into(binding.layoutTeam2.ivTeamIcon);

        setUpResult();
    }

    private void setUpBestPlayers() {
        binding.bestPlayerStats.layoutMom.tvTitle.setText("Man of the match");
        binding.bestPlayerStats.layoutBom.tvTitle.setText("Bowler of the match");
        binding.bestPlayerStats.layoutFom.tvTitle.setText("Fielder of the match");


        binding.bestPlayerStats.layoutMom.tvValue.setText(matchData.getManOfMatch());
        binding.bestPlayerStats.layoutBom.tvValue.setText(matchData.getBowlerOfMatch());
        binding.bestPlayerStats.layoutFom.tvValue.setText(matchData.getBestFielding());
    }


    private void setUpResult() {
        if (matchData.getTeamA().equals(matchData.getWinner())) {
            binding.layoutTeam1.cardResult.setCardBackgroundColor(getResources().getColor(R.color.colorWonGreen));
            binding.layoutTeam1.tvResult.setText("Winner");
        }


        if (matchData.getTeamB().equals(matchData.getWinner())) {
            binding.layoutTeam2.cardResult.setCardBackgroundColor(getResources().getColor(R.color.colorWonGreen));
            binding.layoutTeam2.tvResult.setText("Winner");
        }


        if (matchData.getTeamA().equals(matchData.getLooser())) {
            binding.layoutTeam1.cardResult.setCardBackgroundColor(getResources().getColor(R.color.colorLoseRed));
            binding.layoutTeam1.tvResult.setText("Looser");
        }


        if (matchData.getTeamB().equals(matchData.getLooser())) {
            binding.layoutTeam2.cardResult.setCardBackgroundColor(getResources().getColor(R.color.colorLoseRed));
            binding.layoutTeam2.tvResult.setText("Looser");
        }
    }
}
